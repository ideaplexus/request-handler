<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\HandlerController;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

/**
 * @internal
 */
#[CoversClass('App\Controller\HandlerController')]
final class HandlerControllerTest extends WebTestCase
{
    protected const HOST = 'localhost:10000';

    private static Process $process;

    public static function setUpBeforeClass(): void
    {
        self::$process = new Process(['php', '-S', self::HOST, '-t', realpath(__DIR__ . '/../_data')]);
        self::$process->start();

        // wait for server to get going
        while (!self::$process->isRunning()) {
            usleep(100000);
        }
    }

    public static function tearDownAfterClass(): void
    {
        self::$process->stop();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        restore_exception_handler();
    }

    public static function provideBasicRequestCases(): iterable
    {
        $xml = <<<'XML'
            <?xml version="1.0" encoding="UTF-8" ?>
            <root>
            </root>

            XML;

        $html = <<<'HTML'
            <!DOCTYPE html>
            <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>html</title>
            </head>
            <body></body>
            </html>

            HTML;

        return [
            'GET request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'POST request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'POST',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'PUT request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'POST',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'HEAD request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'HEAD',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'DELETE request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'DELETE',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'PATCH request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'PATCH',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'OPTIONS request on / without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'PATCH',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on /some/url without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/some/url',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on /some/url?with=parameter without Accept and Content-Type header' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/some/url?with=parameter',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on / without Accept and Content-Type header but with content' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => 'unit test',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on / without Accept and Content-Type header with explicit X-MODE "empty"' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => '', 'HTTP_X-MODE' => 'empty'],
                    'content' => 'unit test',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on / with Accept header' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'text/plain', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on / with Content-Type header' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => 'text/plain'],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => '',
                ],
            ],
            'GET request on / with Accept header of type application/json' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'application/json', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'application/json'],
                    'content' => '{}',
                ],
            ],
            'GET request on / with Content-Type header of type application/json' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => 'application/json'],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'application/json'],
                    'content' => '{}',
                ],
            ],
            'GET request on / with Accept header of type application/xml' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'application/xml', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'application/xml'],
                    'content' => "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<root>\n</root>\n",
                ],
            ],
            'GET request on / with Content-Type header of type application/xml' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => 'application/xml'],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'application/xml'],
                    'content' => $xml,
                ],
            ],
            'GET request on / with Accept header of type text/xml' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'text/xml', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/xml; charset=UTF-8'],
                    'content' => $xml,
                ],
            ],
            'GET request on / with Content-Type header of type text/xml' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => 'text/xml'],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/xml; charset=UTF-8'],
                    'content' => $xml,
                ],
            ],
            'GET request on / with Accept header of type text/html' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'text/html', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/html; charset=UTF-8'],
                    'content' => $html,
                ],
            ],
            'GET request on / with Content-Type header of type text/html' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => 'text/html'],
                    'content' => '',
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/html; charset=UTF-8'],
                    'content' => $html,
                ],
            ],
            'POST request on / with X-MODE "mirror" without content type' => [
                'request' => [
                    'method' => 'POST',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => 'some content',
                    'configure' => [
                        'mode' => HandlerController::MODE_MIRROR,
                    ],
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => ''],
                    'content' => 'some content',
                ],
            ],
            'POST request on / with X-MODE "mirror" with Accept header of type text/plain' => [
                'request' => [
                    'method' => 'POST',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => 'text/plain', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => 'some content',
                    'configure' => [
                        'mode' => HandlerController::MODE_MIRROR,
                    ],
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/plain; charset=UTF-8'],
                    'content' => 'some content',
                ],
            ],
            'GET request on / with X-MODE "proxy" with content type header of type text/html' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                    'configure' => [
                        'mode' => HandlerController::MODE_PROXY,
                        'target' => \sprintf('http://%s/html.php', self::HOST),
                    ],
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/html; charset=UTF-8'],
                    'content' => file_get_contents(__DIR__ . '/../_data/html.html'),
                ],
            ],
            'GET request on /?query=parameter with X-MODE "proxy" with content type header of type text/html' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/?query=parameter',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                    'configure' => [
                        'mode' => HandlerController::MODE_PROXY,
                        'target' => \sprintf('http://%s/html.php', self::HOST),
                    ],
                ],
                'response' => [
                    'statusCode' => 200,
                    'headers' => ['Content-Type' => 'text/html; charset=UTF-8'],
                    'content' => file_get_contents(__DIR__ . '/../_data/html.html'),
                ],
            ],
            'GET request on / with X-MODE "proxy" with invalid target' => [
                'request' => [
                    'method' => 'GET',
                    'uri' => '/',
                    'parameters' => [],
                    'files' => [],
                    'server' => ['HTTP_ACCEPT' => '', 'HTTP_CONTENT_TYPE' => ''],
                    'content' => '',
                    'configure' => [
                        'mode' => HandlerController::MODE_PROXY,
                        'target' => 'invalidtarget',
                    ],
                    'configure_issue' => true,
                ],
                'response' => [
                    'statusCode' => 400,
                    'headers' => ['Content-Type' => 'text/html; charset=UTF-8'],
                    'content' => null,
                ],
            ],
        ];
    }

    /**
     * @param array $request  request parameters
     * @param array $response expected response data
     */
    #[DataProvider('provideBasicRequestCases')]
    public function testBasicRequest(array $request, array $response): void
    {
        $client = self::createClient();

        // preflight: configure request-handler
        $default = ['mode' => HandlerController::MODE_EMPTY, 'target' => ''];
        $client->request('GET', '/_configure?' . http_build_query($request['configure'] ?? $default));
        if ($request['configure_issue'] ?? false) {
            // if an issue during configuration was found (as intended), return
            self::assertStringContainsString('error', $client->getResponse()->getContent());

            return;
        }

        // do request
        list($method, $uri, $parameters, $files, $server, $content) = array_values($request);
        $client->request($method, $uri, $parameters, $files, $server, $content);

        // verify response
        list($statusCode, $headers, $content) = array_values($response);
        self::assertSame($statusCode, $client->getResponse()->getStatusCode());

        if (null !== $content) {
            self::assertSame($content, $client->getResponse()->getContent());
        }

        foreach ($headers as $header => $value) {
            self::assertSame($value, $client->getResponse()->headers->get($header));
        }
    }
}
