<?php

$finder = PhpCsFixer\Finder::create()
    ->in('src')
;

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude(['config', 'var'])
    ->notContains('/Kernel/')
;


$config = new PhpCsFixer\Config();
$config
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony:risky' => true,
        '@PHP84Migration' => true,
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@PSR12:risky' => true,
        'list_syntax' => ['syntax' => 'long'],
        'concat_space' => ['spacing' => 'one'],
    ])
    ->setFinder($finder)
;

return $config;
