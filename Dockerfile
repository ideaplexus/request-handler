ARG PROXY_CACHE_PREFIX

FROM ${PROXY_CACHE_PREFIX}php:8.4-alpine

COPY bin config public src templates vendor composer.json composer.lock /app/

WORKDIR /app

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk update --no-cache \
  && apk add --no-cache --virtual .build-deps \
    bash=~5.2 linux-headers=~6.6 \
  && chmod +x /usr/local/bin/install-php-extensions \
  && install-php-extensions intl xdebug \
  && docker-php-ext-enable intl xdebug \
  && curl --tlsv1.2 -sSf https://raw.githubusercontent.com/composer/getcomposer.org/main/web/installer  | php -- --quiet \
  && COMPOSER_ALLOW_SUPERUSER=1 php composer.phar install --no-cache --no-interaction --no-scripts --prefer-dist --classmap-authoritative \
  && rm -f composer.phar \
  && curl --tlsv1.2 -sSf https://get.symfony.com/cli/installer | bash -s -- --install-dir /usr/local/bin/ \
  && apk del .build-deps

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/symfony", "serve", "--no-tls", "--port=8080"]
