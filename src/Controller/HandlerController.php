<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\AcceptHeader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HandlerController extends AbstractController
{
    public const MODE_MIRROR = 'mirror';
    public const MODE_PROXY = 'proxy';
    public const MODE_EMPTY = 'empty';

    private const CONTENT_TYPE_TEXT_PLAIN = 'text/plain';
    private const CONTENT_TYPE_TEXT_HTML = 'text/html';
    private const CONTENT_TYPE_TEXT_XML = 'text/xml';
    private const CONTENT_TYPE_APPLICATION_XML = 'application/xml';
    private const CONTENT_TYPE_APPLICATION_JSON = 'application/json';

    #[Route(path: '/{path}', name: 'index', requirements: ['path' => '.*'])]
    final public function index(
        Request $request,
        HttpClientInterface $client,
        CacheItemPoolInterface $cache,
    ): Response {
        $modeItem = $cache->getItem('mode');
        $targetItem = $cache->getItem('target');

        return match ($modeItem->get()) {
            self::MODE_MIRROR => $this->handleModeMirror($request),
            self::MODE_PROXY => $this->handleModeProxy(
                $request,
                $client,
                \is_string($targetItem->get()) ? $targetItem->get() : ''
            ),
            default => $this->handleModeEmpty($request),
        };
    }

    #[Route(path: '/_configure', name: 'configure', methods: ['GET'], priority: 1)]
    final public function configure(
        Request $request,
        CacheItemPoolInterface $cache,
        ValidatorInterface $validator,
    ): Response {
        /** @var array<string, array<string, string>> $response */
        $response = [];

        $modeItem = $cache->getItem('mode');
        $modeValueCached = \is_string($modeItem->get()) ? $modeItem->get() : self::MODE_EMPTY;
        $modeValue = $request->query->get('mode', $modeValueCached);

        $targetItem = $cache->getItem('target');
        $targetValueCached = \is_string($targetItem->get()) ? $targetItem->get() : '';
        $targetValue = $request->query->get('target', $targetValueCached);

        $choiceConstraint = new Assert\Choice(
            choices: [self::MODE_EMPTY, self::MODE_MIRROR, self::MODE_PROXY],
            message: '{{ value }} is not a valid value for "mode".'
        );
        $errors = $validator->validate($modeValue, $choiceConstraint);
        if ($errors->count() > 0) {
            $response['error']['mode'] = $errors->get(0)->getMessage();
        }

        if (self::MODE_PROXY === $modeValue) {
            $blankConstraint = new Assert\NotBlank(message: '"target" must not be empty.');
            $urlConstraint = new Assert\Url(message: '{{ value }} is not a valid url for "target".');
            $errors = $validator->validate($targetValue, [$blankConstraint, $urlConstraint]);
            if ($errors->count() > 0) {
                $response['error']['target'] = $errors->get(0)->getMessage();
            }
        }

        if (\array_key_exists('error', $response)) {
            $modeValue = $modeValueCached;
            $targetValue = $targetValueCached;
        } else {
            $modeItem->set($modeValue);
            $modeItem->expiresAfter(null);
            $targetItem->set($targetValue);
            $targetItem->expiresAfter(null);
            $cache->save($modeItem);
            $cache->save($targetItem);
        }

        $response['mode'] = $modeValue;
        $response['target'] = $targetValue;

        return new JsonResponse($response);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function handleModeProxy(Request $request, HttpClientInterface $client, string $target): Response
    {
        $headers = static function (array $headers): array {
            foreach ($headers as $k => $v) {
                if (!\is_array($v)) {
                    continue;
                }
                $headers[$k] = $v[array_key_first($v)];
            }

            return $headers;
        };

        $qs = $request->getQueryString();
        if (null !== $qs) {
            $qs = '?' . $qs;
        }

        $targetResponse = $client->request(
            $request->getMethod(),
            $target . $request->getPathInfo() . $qs,
            ['headers' => ['User-Agent' => $_SERVER['HTTP_USER_AGENT'] ?? '']]
        );

        /** @var array<string, string> $targetHeaders */
        $targetHeaders = $headers($targetResponse->getHeaders(false));

        unset($targetHeaders['content-length'], $targetHeaders['content-encoding']);

        return new Response($targetResponse->getContent(false), $targetResponse->getStatusCode(), $targetHeaders);
    }

    private function handleModeMirror(Request $request): Response
    {
        $headers = $request->headers->all();
        // set content-type from accept type
        $headers['content-type'] = $headers['accept'] ?? [self::CONTENT_TYPE_TEXT_PLAIN];

        return new Response($request->getContent(), 200, $headers);
    }

    private function handleModeEmpty(Request $request): Response
    {
        // check if any accept header was set
        $targetResponse = $this->getResponse(AcceptHeader::fromString($request->headers->get('Accept')));
        if (null !== $targetResponse) {
            return $targetResponse;
        }

        // if not, check the content type
        $targetResponse = $this->getResponse(AcceptHeader::fromString($request->headers->get('Content-Type')));
        if (null !== $targetResponse) {
            return $targetResponse;
        }

        // if nothing worked, send text response
        return $this->textResponse();
    }

    /**
     * Return a response.
     */
    private function getResponse(AcceptHeader $acceptHeader): ?Response
    {
        $response = null;

        foreach ($acceptHeader->all() as $acceptHeaderItem) {
            switch ($acceptHeaderItem->getValue()) {
                case self::CONTENT_TYPE_TEXT_PLAIN:
                    $response = $this->textResponse();

                    break 2;

                case self::CONTENT_TYPE_TEXT_HTML:
                    $response = $this->htmlResponse();

                    break 2;

                case self::CONTENT_TYPE_TEXT_XML:
                case self::CONTENT_TYPE_APPLICATION_XML:
                    $response = $this->xmlResponse($acceptHeaderItem->getValue());

                    break 2;

                case self::CONTENT_TYPE_APPLICATION_JSON:
                    $response = $this->jsonResponse();

                    break 2;

                default:
                    break;
            }
        }

        return $response;
    }

    /**
     * Return a HTML response.
     */
    private function htmlResponse(): Response
    {
        return $this->render('base.html.twig');
    }

    /**
     * Return a XML response.
     */
    private function xmlResponse(string $contentType): Response
    {
        $response = $this->render('base.xml.twig');
        $response->headers->set('Content-Type', $contentType);

        return $response;
    }

    /**
     * Return a JSON response.
     */
    private function jsonResponse(): JsonResponse
    {
        return new JsonResponse();
    }

    /**
     * Return a text response.
     */
    private function textResponse(): Response
    {
        return new Response('', 200, ['Content-Type' => self::CONTENT_TYPE_TEXT_PLAIN]);
    }
}
