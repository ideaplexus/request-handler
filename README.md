# Request Handler

[![pipeline status](https://gitlab.com/ideaplexus/php/request-handler/badges/master/pipeline.svg)](https://gitlab.com/ideaplexus/php/request-handler/-/commits/master)
[![coverage report](https://gitlab.com/ideaplexus/php/request-handler/badges/master/coverage.svg)](https://gitlab.com/ideaplexus/php/request-handler/-/commits/master)

Simple symfony app that answers requests in different modes

## Usage

The Request Handler supports three modes: `mirror`, `proxy` and `empty` while `empty` is the fallback mode. The modes can be activated by adding a request header called `X-Mode`.

### Mode `empty`

The `empty` mode will return an empty response. Based on the request headers the response body can have different content. 
The `Accept` header and the `Content-Type` header as fallback will be checked for the supported types `application/json`, `application/xml`, `text/xml`, `text/html`, and `text/plain`.

#### Responses

##### JSON

For the type `application/json` an empty json will be returned. The `Content-Type` response header will be set to `application/json`.

```json
{}
```

##### XML

For the types `application/xml` or `text/xml` a xml document with an empty root node will be returned. The `Content-Type` response header will be set to the incoming type.

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<root>
</root>
```

##### HTML

For the type `text/html` a html document with an empty body will be returned. The `Content-Type` response header will be set to `text/html`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>html</title>
</head>
<body></body>
</html>
```

#### TEXT

For the type `text/plain` and as fallback - in case of no/any other header - a response without content will be sent. The `Content-Type` response header will be set to `text/plain`.

## Mode `mirror`

The mirror mode just return the content sent to the Request Handler. In addition, if the request contains an `Accept` header, the `Content-Type` response header will be set to this header.

### Mode `proxy`

The proxy mode requires an additional request header called `X-Target`. With this header you define witch page should be requested.

### Docker

```bash
docker run -it --rm -p 8080:8080 ideaplexus/request-handler
```
